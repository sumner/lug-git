# lug-git-talk
A presentation covering a bunch of Git. Hosted on GitLab. For the Mines Linux Users Group (LUG).

## Topics
- What is Git and why you should use it
- How to install Git and use it locally
- Merging
- Branches
- Cool Tips
